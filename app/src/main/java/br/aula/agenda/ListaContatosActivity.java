package br.aula.agenda;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import br.aula.agenda.utils.ApiClient;
import br.aula.agenda.utils.ApiServices;
import br.aula.agenda.utils.Contatos;
import br.aula.agenda.bd.BancoDadosHelper;
import br.aula.agenda.bd.Contato;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListaContatosActivity extends AppCompatActivity {

    private ListView listaContatos;
    private List<Contato> contatos;
    private Contato contatoSelecionado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listacontatos);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        myToolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(myToolbar);

        BancoDadosHelper bd = new BancoDadosHelper(ListaContatosActivity.this);
        contatos = bd.listaContatos();
        bd.close();

        ArrayAdapter<Contato> adapter = new ArrayAdapter<Contato>(this, android.R.layout.simple_list_item_1, contatos);

        listaContatos = (ListView) findViewById(R.id.lista);
        listaContatos.setAdapter(adapter);

        listaContatos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int posicao, long id) {
                Intent intent = new Intent(ListaContatosActivity.this, ContatoActivity.class);
                intent.putExtra("contato", contatos.get(posicao));
                startActivity(intent);
            }
        });

        listaContatos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapter, View view, int posicao, long id) {
                contatoSelecionado = contatos.get(posicao);
                return false;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.novo:
                Intent intent = new Intent(this, ContatoActivity.class);
                startActivity(intent);
                return false;

            case R.id.sincronizar:
                Toast.makeText(this, "Sincronizando", Toast.LENGTH_LONG).show();
                return false;

            case R.id.receber:
                new LendoServidorTask().execute();
                return false;

            case R.id.enviar:
                new EnviandoServidorTask().execute();
                return false;

            case R.id.mapa:
                Intent mapa = new Intent(ListaContatosActivity.this, MapaFragment.class);
                startActivity(mapa);
                return false;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_contato_contexto, menu);
        super.onCreateContextMenu(menu, v, menuInfo);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.excluir:

                new AlertDialog.Builder(ListaContatosActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Deletar")
                        .setMessage("Deseja mesmo deletar ?")
                        .setPositiveButton("Quero",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        BancoDadosHelper dao = new BancoDadosHelper(ListaContatosActivity.this);
                                        dao.apagaContato(contatoSelecionado);
                                        dao.close();
                                        carregaLista();
                                    }
                                }).setNegativeButton("Nao", null).show();

                return false;

            case R.id.enviasms:
                Intent intentSms = new Intent(Intent.ACTION_VIEW);
                intentSms.setData(Uri.parse("sms:"+ contatoSelecionado.getTelefone()));
                intentSms.putExtra("sms_body", "Mensagem");
                item.setIntent(intentSms);
                return false;

            case R.id.enviaemail:
                Intent intentEmail = new Intent(Intent.ACTION_SEND);
                intentEmail.setType("message/rfc822");
                intentEmail.putExtra(Intent.EXTRA_EMAIL,
                        new String[] {contatoSelecionado.getEmail()});
                intentEmail.putExtra(Intent.EXTRA_SUBJECT, "Teste de email");
                intentEmail.putExtra(Intent.EXTRA_TEXT, "Corpo da mensagem");
                //item.setIntent(intentEmail);
                startActivity(Intent.createChooser(intentEmail, "Selecione a sua aplicação de Email"));

                return false;

            case R.id.ligar:
                Intent intentLigar = new Intent(Intent.ACTION_DIAL);
                intentLigar.setData(Uri.parse("tel:"+ contatoSelecionado.getTelefone()));
                item.setIntent(intentLigar);
                return false;

            case R.id.share:
                Intent intentShare = new Intent(Intent.ACTION_SEND);
                intentShare.setType("text/plain");
                intentShare.putExtra(Intent.EXTRA_SUBJECT, "Assunto que será compartilhado");
                intentShare.putExtra(Intent.EXTRA_TEXT, "Texto que será compartilhado");
                startActivity(Intent.createChooser(intentShare, "Escolha como compartilhar"));
                return false;

            case R.id.visualizarmapa:
                Uri gmmIntentUri = Uri.parse("geo:0,0?q="+ contatoSelecionado.getEndereco());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
                return false;


            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        BancoDadosHelper bd = new BancoDadosHelper(ListaContatosActivity.this);
        contatos = bd.listaContatos();
        bd.close();

        ArrayAdapter<Contato> adapter = new ArrayAdapter<Contato>(this, android.R.layout.simple_list_item_1, contatos);

        listaContatos = (ListView) findViewById(R.id.lista);
        listaContatos.setAdapter(adapter);

        registerForContextMenu(listaContatos);
    }

    private void carregaLista() {
        BancoDadosHelper bd = new BancoDadosHelper(ListaContatosActivity.this);
        contatos = bd.listaContatos();
        bd.close();
        ArrayAdapter<Contato> adapter = new ArrayAdapter<Contato>(this, android.R.layout.simple_list_item_1, contatos);
        listaContatos.setAdapter(adapter);
    }

    private class LendoServidorTask extends AsyncTask<Object, Object, Boolean> {

        public static final String URLSERVIDOR = "http://192.168.0.108:8080/";
        private ProgressDialog progress;
        private ApiServices apiServices;

        @Override
        protected Boolean doInBackground(Object... params) {

            apiServices = ApiClient.getClient(URLSERVIDOR).create(ApiServices.class);

            Call<Contatos> call = apiServices.findAll("application/json");
            call.enqueue(new Callback<Contatos>() {
                @Override
                public void onResponse(Call<Contatos> call, Response<Contatos> response) {
                    if (response.isSuccessful()) {

                        Contatos contatos = response.body();
                        if(contatos != null){
                            BancoDadosHelper bd = new BancoDadosHelper(ListaContatosActivity.this);
                            for (Contato contato : contatos.getContatos()) {
                                bd.insereContato(contato);
                            }
                            bd.close();
                            carregaLista();
                        }
                    }
                }
                @Override
                public void onFailure(Call<Contatos> call, Throwable t) {
                    t.printStackTrace();
                }
            });

            return true;
        }

        @Override
        protected void onPreExecute() {
            // executa algo antes de iniciar a tarefa
            progress = ProgressDialog.show(ListaContatosActivity.this, "Aguarde ...", "Recebendo informações da web", true, true);
        }

        @Override
        protected void onPostExecute(Boolean isOk) {
            if(isOk){
                progress.dismiss();
            }
        }
    }

    private class EnviandoServidorTask extends AsyncTask<Object, Object, String> {

        public static final String URLSERVIDOR = "http://192.168.0.104:8080/ContatosWeb/RecebeServlet";
        private ProgressDialog progress;

        @Override
        protected String doInBackground(Object... params) {

            try {
                JSONObject jsonObject = new JSONObject();
                JSONArray array = new JSONArray();

                BancoDadosHelper bd = new BancoDadosHelper( ListaContatosActivity.this);
                for (Contato contato : bd.listaContatos()) {
                    JSONObject contatoJson = new JSONObject();
                    contatoJson.put("nome", contato.getNome());
                    contatoJson.put("endereco", contato.getEndereco());
                    array.put(contatoJson);
                }

                jsonObject.put("contatos",array);
                bd.close();

                Log.i("aula",jsonObject.toString());
                postData(URLSERVIDOR, jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPreExecute() {
            // executa algo antes de iniciar a tarefa
            progress = ProgressDialog.show(ListaContatosActivity.this, "Aguarde ...", "Enviando as informações para a web", true, true);
        }

        @Override
        protected void onPostExecute(String result) {
            progress.dismiss();
        }

        private void postData(String url, JSONObject obj) {
            // Create a new HttpClient and Post Header

            HttpParams myParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(myParams, 10000);
            HttpConnectionParams.setSoTimeout(myParams, 10000);
            HttpClient httpclient = new DefaultHttpClient(myParams);
            String json = obj.toString();

            try {
                HttpPost httppost = new HttpPost(url.toString());
                httppost.setHeader("Content-type", "application/json");
                StringEntity se = new StringEntity(json);
                se.setContentEncoding(new BasicHeader("content-type", "application/json"));
                httppost.setEntity(se);
                HttpResponse response = httpclient.execute(httppost);
                String temp = EntityUtils.toString(response.getEntity());
                Log.i("tag", temp);

            } catch (ClientProtocolException e) {
                Log.i("tag", e.getMessage());
            } catch (IOException e) {
                Log.i("tag", e.getMessage());
            }
        }

    }
}