package br.aula.agenda;

/**
 * Created by cubas on 15/08/17.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

import br.aula.agenda.bd.BancoDadosHelper;

public class SMSReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle bundle = intent.getExtras();
        Object messages[] = (Object[]) bundle.get("pdus");
        SmsMessage smsMessage[] = new SmsMessage[messages.length];

        for (int n = 0; n < messages.length; n++) {
            smsMessage[n] = SmsMessage.createFromPdu((byte[]) messages[n]);
        }

        BancoDadosHelper helper = new BancoDadosHelper(context);
        if (helper.isContato(smsMessage[0].getDisplayOriginatingAddress())) {
            Toast.makeText(context, "SMS Contato" + smsMessage[0].getMessageBody(), Toast.LENGTH_LONG).show();

            MediaPlayer mp = MediaPlayer.create(context, R.raw.gol4);
            mp.start();
        }
        helper.close();
    }

}