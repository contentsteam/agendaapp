package br.aula.agenda.utils;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface ApiServices {

    @GET("/ContatosWeb/ContatoServlet")
    Call<Contatos> findAll(@Header("Content-Type") String content_type);

}
