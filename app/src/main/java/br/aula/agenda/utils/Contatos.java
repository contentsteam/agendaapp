package br.aula.agenda.utils;

import java.util.ArrayList;
import java.util.List;

import br.aula.agenda.bd.Contato;

public class Contatos {

    private List<Contato> contatos = new ArrayList<>();

    public List<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contato> contatos) {
        this.contatos = contatos;
    }
}
