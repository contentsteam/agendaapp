package br.aula.agenda;

/**
 * Created by cubas on 16/08/17.
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import br.aula.agenda.bd.BancoDadosHelper;
import br.aula.agenda.bd.Contato;

public class MapaFragment extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_layout);

        Toolbar myChildToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myChildToolbar);

        // Get a support ActionBar corresponding to this toolbar
        android.support.v7.app.ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);


        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        mMap.getUiSettings().setZoomControlsEnabled(true);



        /** NOVO CODIGO **/
        BancoDadosHelper bd = new BancoDadosHelper(this);
        List<Contato> contatos = bd.listaContatos();
        bd.close();

        Localizador localizador = null;
        LatLng coordenada = null;
        for (Contato contato : contatos) {

            localizador = new Localizador(this);
            coordenada = localizador.geoCoordenada(contato.getEndereco());

            if(coordenada != null){

                MarkerOptions marcador = new MarkerOptions().position(coordenada)
                        .title(contato.getNome()).snippet(contato.getEndereco());

                mMap.addMarker(marcador);
            }
        }

        //Localizador coderUtil = new Localizador(this);
        //LatLng local = coderUtil.geoCoordenada("St James's Gate, Ushers, Dublin 8, Irlanda");
        //mMap.addMarker(new MarkerOptions().position(local).title("Guinness Storehouse"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(local));


    }

}