package br.aula.agenda;

/**
 * Created by cubas on 16/08/17.
 */


import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


import br.aula.agenda.bd.Contato;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ContatoUtil {

    public static String acessar(String endereco)
    {
        try {

            URL url = new URL(endereco);
            URLConnection conn = url.openConnection();
            InputStream is = conn.getInputStream();
            Scanner scanner = new Scanner(is);
            String conteudo = scanner.useDelimiter("\\A").next();
            scanner.close();
            return conteudo;

        } catch (Exception e){
            return null;
        }
    }

    public static List<Contato> contatoConverter(String conteudo) throws JSONException {

        List<Contato> contatos = new ArrayList<Contato>();

        JSONObject jsonObject = new JSONObject(conteudo);
        JSONArray listaContatos = jsonObject.getJSONArray("contatos");

        for (int i = 0; i < listaContatos.length(); i++) {
            JSONObject j = listaContatos.getJSONObject(i);
            Contato c = new Contato();
            c.setNome(j.getString("nome"));
            c.setEndereco(j.getString("endereco"));
            c.setTelefone(j.getString("telefone"));
            c.setDataNascimento(j.getLong("dataNascimento"));
            c.setEmail(j.getString("email"));
            c.setSite(j.getString("site"));;
            contatos.add(c);
        }
        return contatos;
    }



}